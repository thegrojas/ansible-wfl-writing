<div align="center">

<p align="center">
  <a href="" rel="noopener">
    <img width=200px height=200px src="https://upload.wikimedia.org/wikipedia/commons/2/24/Ansible_logo.svg" alt="Ansible logo"></a>
  </a>
</p>

  [![Status](https://img.shields.io/badge/status-Active-success.svg)]() 
  [![Ansible Galaxy](https://img.shields.io/ansible/role/TODO)]() 
  [![Ansible Version](https://img.shields.io/badge/ansible-2.10-informational)]() 
  [![Ansible Version](https://img.shields.io/ansible/quality/TODO)]() 
  [![License](https://img.shields.io/badge/license-GPLv3-blue.svg)](/LICENSE)

</div>

---

# Ansible Workflow - Writing

Ansible role that sets up a writing workflow alogn with all the tools needed.

This role performs the following tasks:

- Installs LaTex
- Installs Pandoc
- Changes LaTex Home variable
- Install RevealJS
- TODO: Installs Pandoc Template Package
- TODO: Installs Zotero and Zotero's BibTex Plugin

## 🦺 Requirements

*None*

## 🗃️ Role Variables

*None*

## 📦 Dependencies

*None*

## 🤹 Example Playbook

*TODO*

## ⚖️ License

This code is released under the [GPLv3](./LICENSE) license. For more information refer to `LICENSE.md`

## ✍️ Author Information

Gerson Rojas <thegrojas@protonmail.com>

## References

- [Setting up a Scholarly Writing Enviroment VsCodium and Pandoc](https://www.youtube.com/watch?v=J86Pm62XM_Q)
